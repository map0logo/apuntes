# Apuntes del Curso de Teoría de Colas e Inventarios

Estos apuntes toman como punto de partida el libro *Operations Research: A Model-Based Approach* de H. A. Eiselt y Carl-Louis Sandblom. Pero se irá modificando desde un punto de vista crítico personal, y la experiencia ganada tras años de docencia.

Se destacan los libros auxiliares: 

* *Applied Probability and Stochastic Processes Second Edition* por Richard M. Feldman y Ciriaco Valdez-Flores 
* *Introducción a la Investigación de Operaciones* Novena edición Frederick S. Hillier y Gerald J. Lieberman.
* *Operations research and management science handbook* editor, A. Ravi Ravindran.

El programa de este curso comprende cuatro temas básicos:

* Cadenas de Markov
* Teoría de Colas
* Modelos de Inventarios
* Simulación

Se emplea un enfoque desde la programación, presentando los temas relacionados desde cuadernos de Jupyter y scripts en Python.