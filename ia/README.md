# Apuntes del Inteligencia Artificial

Estos apuntes toman como punto de partida el libro *Artificial Intelligence A Modern Approach Third Edition* de Stuart J. Russell y Peter Norvig. Pero se irá modificando desde un punto de vista crítico personal.

El programa de este curso comprende cuatro temas básicos:

* Búsqueda
    * desinformada
    * informada
    * competitiva
    * lógica
* Razonamiento bajo incertidumbre
    * Redes Bayesianas
    * Teoría de Decisión
    * Aprendizaje automático

Se emplea un enfoque desde la programación, presentando los temas relacionados desde cuadernos de Jupyter y scripts en Python.